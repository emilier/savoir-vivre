<?php

use Illuminate\Database\Seeder;

class QuestionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('questions')->insert([
            "created_at" => "2018-11-26",
            "updated_at" => "2018-11-26",
            "name" => "Dans la rue, où se tient un homme qui accompagne une femme ?",
            "explanation" => "L'homme se place à côté de la femme en fonction du trottoir, laissant à celle-ci le &quot;haut du pavé&quot; pour la protéger des éclaboussures. Survivance d'une époque où les élégantes de la haute société devaient éviter les rigoles peu salubres des rues d'autrefois. D'où l'espression &quot;tenir le haut du pavé&quot;. En revanche, si deux hommes accompagnent une ou deux femmes, ces messieurs se placeront de part et d'autres de ces dames.",
            "rule" => "1",
            "difficulty_id" => "2",
            "views" => "384",
            "favourites" => "484",
            "votes" => "561",
            "rating" => "1.3",
            "sharing" => "509",
            "source" => "",
        ]);

        DB::table('questions')->insert([
            "created_at" => "2019-02-27",
            "updated_at" => "2019-02-27",
            "name" => "Dans une voiture, quelle place le conducteur accorde-t-il à une femme qui est sa passagère ?",
            "explanation" => "Si c'est une dame âgée, on peut lui proposer la place de derrière à droite qui est la place d'honneur dans les voitures officielles. Qu'elle soit jeune ou non, l'homme lui ouvrira la portière. Non seulement aux premiers temps d'une liaison, mais cinquante années après, si celle-ci dure encore !",
            "rule" => "1",
            "views" => "462",
            "favourites" => "162",
            "votes" => "145",
            "rating" => "1.6",
            "sharing" => "859",
            "source" => "",
        ]);

        DB::table('questions')->insert([
            "created_at" => "2018-11-29",
            "updated_at" => "2018-11-29",
            "name" => "Pour monter ou descendre un escalier, comment l'homme se place-t-il par rapport à la femme ?",
            "explanation" => "Par discrétion en montant, et pour la rattraper en descendant, si nécessaire ! En agleterre, l'homme se met derrière à la montée.",
            "views" => "534",
            "favourites" => "553",
            "votes" => "590",
            "rating" => "1.2",
            "sharing" => "522",
            "source" => "",
        ]);

        DB::table('questions')->insert([
            "created_at" => "2018-09-29",
            "updated_at" => "2018-09-29",
            "name" => "Un homme doit-il se lever dès qu'une femme rentre dans une pièce ?",
            "explanation" => "Un homme se lève dès qu'une femme entre dans une pièce, ou dès qu'elle se lève elle-même si elle était déjà dans la pièce.",
            "views" => "21",
            "favourites" => "233",
            "votes" => "288",
            "rating" => "1.6",
            "sharing" => "19",
            "source" => "",
        ]);

        DB::table('questions')->insert([
            "created_at" => "2018-12-13",
            "updated_at" => "2018-12-13",
            "name" => "Est-il inconvenant qu'un homme fasse un compliment à une femme sur sa tenue ?",
            "explanation" => "Il peut faire un compliment à condition qu'il ne soit pas appuyé.",
            "views" => "569",
            "favourites" => "258",
            "votes" => "218",
            "rating" => "1.6",
            "sharing" => "192",
            "source" => "",
        ]);

        DB::table('questions')->insert([
            "created_at" => "2018-10-23",
            "updated_at" => "2018-10-23",
            "name" => "Votre contrat de travail stipule que vous devez prendre votre poste à 08 h 00. À quelle heure devez-vous arriver chaque matin ?",
            "explanation" => "La ponctualité est une qualité essentielle. Prévoir une marge d’un quart d’heure est nécessaire pour éviter de se laisser piéger dans un embouteillage : les bouchons ne constituent pas une excuse pour arriver en retard. De plus, être en avance sur votre lieu de travail vous permettra de prendre le temps de lier connaissance avec vos collègues : vous vous intégrerez plus naturellement à l’équipe, et de manière conviviale.",
            "views" => "523",
            "favourites" => "114",
            "votes" => "979",
            "rating" => "1.9",
            "sharing" => "301",
            "source" => "http://www.tetranergy.com/blog/quiz-connaissez-vous-les-regles-du-savoir-vivre-en-entreprise/",
        ]);

        DB::table('questions')->insert([
            "created_at" => "2019-01-02",
            "updated_at" => "2019-01-02",
            "name" => "C’est votre jour de repos mais votre chef vous téléphone : votre collègue Gustave est malade, il a besoin de vous pour le remplacer.",
            "explanation" => "Votre chef appréciera votre disponibilité : vous deviendrez un collaborateur précieux à ses yeux. Savoir, parfois, sacrifier de son temps personnel pour l’entreprise est un investissement pour votre carrière. Cela ne signifie pas se laisser exploiter si ces demandes se révèlent abusives.",
            "views" => "0",
            "favourites" => "377",
            "votes" => "175",
            "rating" => "1.8",
            "sharing" => "220",
            "source" => "http://www.tetranergy.com/blog/quiz-connaissez-vous-les-regles-du-savoir-vivre-en-entreprise/",
        ]);

        DB::table('questions')->insert([
            "created_at" => "2018-10-16",
            "updated_at" => "2018-10-16",
            "name" => "Vous venez de boucler la mission qui vous avait été confiée : ouf ! En face de vous, votre collègue Raoul, le petit nouveau, semble débordé.",
            "explanation" => "En aidant votre collègue, vous démontrez votre esprit d’équipe et votre dynamisme : deux qualités très appréciées en entreprise. Raoul, lui, saura vous en être reconnaissant. Mais si vous restez indifférent à son embarras, il n’aura aucune envie de vous aider le jour où vous en aurez besoin. Quant à lui lancer des remarques désobligeantes, c’est le meilleur moyen de vous en faire un ennemi et d’installer une mauvaise ambiance dans le service.",
            "views" => "179",
            "favourites" => "223",
            "votes" => "774",
            "rating" => "1.5",
            "sharing" => "355",
            "source" => "http://www.tetranergy.com/blog/quiz-connaissez-vous-les-regles-du-savoir-vivre-en-entreprise/",
        ]);

        DB::table('questions')->insert([
            "created_at" => "2018-11-05",
            "updated_at" => "2018-11-05",
            "name" => "À la photocopieuse, vous trouvez le contrat de travail de Nicole, oublié sur la vitre. Votre réaction :",
            "explanation" => "Confidentialité et discrétion professionnelle sont de mise en entreprise. Sachez mettre des limites à votre curiosité. La divulgation d’informations ou documents confidentiels peut même constituer une faute professionnelle passible de sanctions.",
            "views" => "933",
            "favourites" => "405",
            "votes" => "537",
            "rating" => "1.7",
            "sharing" => "397",
            "source" => "http://www.tetranergy.com/blog/quiz-connaissez-vous-les-regles-du-savoir-vivre-en-entreprise/",
        ]);

        DB::table('questions')->insert([
            "created_at" => "2019-02-07",
            "updated_at" => "2019-02-07",
            "name" => "Vous vous trouvez face à un client mécontent. Afin de désamorcer le conflit :",
            "explanation" => "En cas de désaccord avec un client (ou un collègue), il est essentiel de savoir se maîtriser afin de ne pas envenimer la relation. Des techniques de communication peuvent vous apprendre à faire preuve d’écoute active et d’adaptabilité pour apaiser votre interlocuteur et rétablir le dialogue.",
            "views" => "425",
            "favourites" => "822",
            "votes" => "200",
            "rating" => "1.9",
            "sharing" => "553",
            "source" => "http://www.tetranergy.com/blog/quiz-connaissez-vous-les-regles-du-savoir-vivre-en-entreprise/",
        ]);

        DB::table('questions')->insert([
            "created_at" => "2019-01-12",
            "updated_at" => "2019-01-12",
            "name" => "Votre responsable vous reçoit en entretien d’évaluation. Elle ou il vous demande ce qui vous motive à travailler dans cette entreprise. Vous lui répondez :",
            "explanation" => "Certes, personne ne travaille gratuitement et votre salaire est important pour vous. Mais ce qu’un dirigeant attend de vous, c’est votre motivation et votre implication dans votre travail, qui déterminent votre volonté de réussir.",
            "views" => "816",
            "favourites" => "308",
            "votes" => "470",
            "rating" => "2",
            "sharing" => "361",
            "source" => "http://www.tetranergy.com/blog/quiz-connaissez-vous-les-regles-du-savoir-vivre-en-entreprise/",
        ]);

        DB::table('questions')->insert([
            "created_at" => "2018-11-06",
            "updated_at" => "2018-11-06",
            "name" => "Vous vous rendez compte, après l’avoir envoyé, que vous avez fait une grosse erreur dans le dossier Duschmoll. Pour vous en sortir :",
            "explanation" => "Personne n’est à l’abri d’une erreur. La dissimuler ou, pire, en accuser quelqu’un d’autre, est non seulement une attitude malhonnête, mais risque d’en aggraver les conséquences. Le comportement responsable, en ce cas, consiste à reconnaître son erreur sans attendre qu’elle soit découverte, et à mettre en œuvre les actions nécessaires pour la réparer ou en atténuer les effets.",
            "views" => "663",
            "favourites" => "766",
            "votes" => "506",
            "rating" => "1.3",
            "sharing" => "735",
            "source" => "http://www.tetranergy.com/blog/quiz-connaissez-vous-les-regles-du-savoir-vivre-en-entreprise/",
        ]);

        DB::table('questions')->insert([
            "created_at" => "2018-10-22",
            "updated_at" => "2018-10-22",
            "name" => "Georgette, une petite nouvelle, a pris ses fonctions dans votre service ce matin. C’est l’heure du déjeuner :",
            "explanation" => "La courtoisie est une vertu précieuse pour rendre la vie agréable au travail. Vous n’êtes pas obligé de nouer des liens d’amitié avec tous vos collègues, mais la convivialité fait partie du savoir-vivre en entreprise, au même titre que d’autres marques de respect des autres comme la politesse, la diplomatie, la patience, la tolérance et l’ouverture d’esprit.",
            "views" => "192",
            "favourites" => "268",
            "votes" => "988",
            "rating" => "1.6",
            "sharing" => "343",
            "source" => "http://www.tetranergy.com/blog/quiz-connaissez-vous-les-regles-du-savoir-vivre-en-entreprise/",
        ]);

        DB::table('questions')->insert([
            "created_at" => "2018-10-04",
            "updated_at" => "2018-10-04",
            "name" => "Est-il approprié de regarder une personne dans les yeux ?",
            "explanation" => "Dans la plupart des pays occidentaux, regarder une personne dans les yeux pendant une conversation est un signe de respect et de sincérité. On dit qu'un contact visuel &quot;normal&quot; dure environ trois secondes. Au-delà, il sera considéré comme &quot;intéressé&quot; : attirance, intérêt intellectuel, il prend contact. En revanche, un regard de moins de trois secondes, comme un regard en douce, est plutôt perçu négativement. L'absence de contact visuel laisse passer un manque de confiance en soi, voire un sentiment de peur. Mais cette perception est loin d'être partagée par tous. Dans de nombreux pays arabes et asiatiques, regarder dans les yeux est perçu comme un manque de respect, voir de la provocation. Il faut au contraire baisser les yeux.",
            "views" => "13",
            "favourites" => "254",
            "votes" => "405",
            "rating" => "1.8",
            "sharing" => "960",
            "source" => "",
        ]);

        DB::table('questions')->insert([
            "created_at" => "2018-10-23",
            "updated_at" => "2018-10-23",
            "name" => "Comment faire pour relancer un conversation ?",
            "explanation" => "Dans un conversation, le silence ne doit pas excéder huit à neuf secondes sous peine de la laisser mourir. Mais comment la relancer ? L'art de la conversation ",
            "views" => "449",
            "favourites" => "440",
            "votes" => "693",
            "rating" => "1.3",
            "sharing" => "916",
            "source" => "",
        ]);

        DB::table('questions')->insert([
            "created_at" => "2019-02-10",
            "updated_at" => "2019-02-10",
            "name" => "Faut-il écrire M. ou Mr ?",
            "explanation" => "Loin d'être interchangeables, &quot;M.&quot; et &quot;Mr&quot; désignent deux réalités distinctes. &quot;Mr&quot; est une abréviation du mot &quot;mister&quot; en anglais, tandis que &quot;M.&quot; est le raccourci de Monsieur en français. L'abbréviation de &quot;Messieurs&quot; s'écrit &quot;MM.&quot;. À savoir aussi que ces raccourcis ne doivent pas être utilisés au début d'un message ou sur une enveloppe.",
            "views" => "66",
            "favourites" => "580",
            "votes" => "831",
            "rating" => "1.6",
            "sharing" => "254",
            "source" => "",
        ]);

        DB::table('questions')->insert([
            "created_at" => "2018-11-06",
            "updated_at" => "2018-11-06",
            "name" => "Faut-il faire la bise ?",
            "explanation" => "Si la bise est très mal perçue dans la plupart des pays, c'est une tradition en France et hommes et femmes qui se connaissent le font à chaque rencontre. Une, deux, trois, quatre : chaque région a sa préférence. Entre collègues, à un niveau hiérarchique équivalent, c'est la personne la plus âgée qui prend l'initiative.  Se faire la bise crée une promiscuité immédiate... Si l'on ne connaît pas bien la personne, on se contente d'effleurer la joue. Habituellement, on présente la joue gauche avant la droite.",
            "views" => "567",
            "favourites" => "693",
            "votes" => "537",
            "rating" => "1.6",
            "sharing" => "747",
            "source" => "",
        ]);

        DB::table('questions')->insert([
            "created_at" => "2018-11-10",
            "updated_at" => "2018-11-10",
            "name" => "Faut-il tutoyer ou vouvoyer ?",
            "explanation" => "Le tutoiement est de plus en plus répandu mais il ne doit pas être systématique. Au contraire, il est préférable de vouvoyer lors d'une d'une première rencontre, même une personne plus jeune que soi. Après un premier contact, c'est en général la personne la plus âgée, en position hiérarchique supérieure ou celle qui reçoit qui va décider : &quot; On pourrait peut-être se dire tu ?&quot;.",
            "views" => "631",
            "favourites" => "881",
            "votes" => "686",
            "rating" => "1.4",
            "sharing" => "364",
            "source" => "",
        ]);

        DB::table('questions')->insert([
            "created_at" => "2019-03-04",
            "updated_at" => "2019-03-04",
            "name" => "Comment faire les présentations ?",
            "explanation" => "La règle est simple : on présente le plus jeune au plus âgé, l'homme à la femme, celui que l'on connaît le moins à celui que l'on connaît le mieux. De la même manière, on présente le subordonné à son supérieur dans un contexte professionnel. Et l'on n'oublie pas : quand on serre la main, elle est franche, et on regarde droit dans les yeux.",
            "views" => "870",
            "favourites" => "367",
            "votes" => "562",
            "rating" => "1.9",
            "sharing" => "349",
            "source" => "",
        ]);

        DB::table('questions')->insert([
            "created_at" => "2018-09-30",
            "updated_at" => "2018-09-30",
            "name" => "Dans quel ordre sont alignés les verres de la gauche vers la droite ?",
            "explanation" => "Le verre d’eau est le plus grand verre, le verre de vin rouge est celui de taille intermédiaire et le plus petit est celui de vin blanc.",
            "views" => "484",
            "favourites" => "885",
            "votes" => "591",
            "rating" => "1.5",
            "sharing" => "506",
            "source" => "",
        ]);

        DB::table('questions')->insert([
            "created_at" => "2018-09-15",
            "updated_at" => "2018-09-15",
            "name" => "Au début du repas, vous avez plein de couteaux et plein de fourchettes... Pour vous y retrouver, par rapport à l’assiette, comment devez-vous les utiliser ?",
            "explanation" => "Il faut commencer le premier plat avec les couverts les plus éloignés de l’assiette.",
            "views" => "776",
            "favourites" => "21",
            "votes" => "655",
            "rating" => "1.7",
            "sharing" => "829",
            "source" => "",
        ]);

        DB::table('questions')->insert([
            "created_at" => "2019-02-05",
            "updated_at" => "2019-02-05",
            "name" => "Pourquoi ne doit-on pas couper les feuilles de salade ?",
            "explanation" => "Les couteaux en argent sont oxydés par le vinaigre. Il était déconseillé de les utiliser car il était difficile de les nettoyer.",
            "views" => "121",
            "favourites" => "668",
            "votes" => "636",
            "rating" => "1.9",
            "sharing" => "111",
            "source" => "",
        ]);

        DB::table('questions')->insert([
            "created_at" => "2018-09-04",
            "updated_at" => "2018-09-04",
            "name" => "Quand vous avez fini de manger, où faut-il placez les couverts ?",
            "explanation" => "Il faut aligner les couverts soit à « 4 h 20 » dans l’assiette, soit de manière parallèle. À l’inverse, il ne faut pas laisser la cuillère du café dans la tasse, mais la reposer sur la coupelle, et ce même lorsqu’on boit le café.",
            "views" => "546",
            "favourites" => "183",
            "votes" => "296",
            "rating" => "1.4",
            "sharing" => "634",
            "source" => "",
        ]);

        DB::table('questions')->insert([
            "created_at" => "2019-02-06",
            "updated_at" => "2019-02-06",
            "name" => "Vous rencontrez quelqu’un pour la première fois. Que lui dites-vous ?",
            "explanation" => "Dire « enchanté(e) » est impoli car la politesse n’aime pas les raccourcis et l’expression vient de l’anglais. De la même manière, « de rien » est à bannir. Privilégiez « je vous en prie ».",
            "views" => "509",
            "favourites" => "398",
            "votes" => "130",
            "rating" => "1.4",
            "sharing" => "583",
            "source" => "",
        ]);

        DB::table('questions')->insert([
            "created_at" => "2018-09-08",
            "updated_at" => "2018-09-08",
            "name" => "Lors d’une conversation téléphonique, qui doit raccrocher en premier ?",
            "explanation" => "Que ce soit pour une conversation téléphonique privée ou professionnelle, celui qui appelle est censée mettre un terme à la conversation.",
            "views" => "37",
            "favourites" => "97",
            "votes" => "310",
            "rating" => "1.3",
            "sharing" => "307",
            "source" => "http://tele7jeux.fr/jeux/test-psycho/connaissez-vous-les-bonnes-manieres/203",
        ]);

        DB::table('questions')->insert([
            "created_at" => "2018-12-08",
            "updated_at" => "2018-12-08",
            "name" => "À table, pour quels aliments ne convient-il pas d’utiliser son couteau ?",
            "explanation" => "Les règles du savoir-vivre en France prévoient qu’il convient de ne pas utiliser son couteau pour couper la salade, les pâtes et l’omelette.",
            "views" => "830",
            "favourites" => "87",
            "votes" => "350",
            "rating" => "1.3",
            "sharing" => "42",
            "source" => "http://tele7jeux.fr/jeux/test-psycho/connaissez-vous-les-bonnes-manieres/203",
        ]);

        DB::table('questions')->insert([
            "created_at" => "2018-11-18",
            "updated_at" => "2018-11-18",
            "name" => "Pour respecter les bonnes manières, il convient de ne pas appeler un interlocuteur que l’on ne connaît pas bien après quelle heure ?",
            "explanation" => "Un sondage TNS Sofrès révèle que la majorité des Français s’accordent sur le fait qu’on ne doit pas appeler un interlocuteur que l’on ne connaît pas bien au-delà de 20h. Pour un ami, ce délai est porté à 21h (jusqu’à 22h pour les moins de 24 ans).",
            "views" => "448",
            "favourites" => "481",
            "votes" => "615",
            "rating" => "1.2",
            "sharing" => "236",
            "source" => "",
        ]);

        DB::table('questions')->insert([
            "created_at" => "2018-10-05",
            "updated_at" => "2018-10-05",
            "name" => "Au restaurant, qui doit passer la porte en premier ?",
            "explanation" => "Au restaurant,  l’homme doit rentrer le premier afin d’éviter tout danger à la femme.",
            "views" => "235",
            "favourites" => "339",
            "votes" => "292",
            "rating" => "1.3",
            "sharing" => "80",
            "source" => "http://tele7jeux.fr/jeux/test-psycho/connaissez-vous-les-bonnes-manieres/203",
        ]);

        DB::table('questions')->insert([
            "created_at" => "2018-12-16",
            "updated_at" => "2018-12-16",
            "name" => "À table, quand peut-on commencer de manger ?",
            "explanation" => "Il convient de s’abstenir de souhaiter « bon appétit » avant de se mettre à table hors du contexte familial. Chacun peut commencer à manger lorsque la maîtresse de maison en donne le signal.",
            "views" => "70",
            "favourites" => "266",
            "votes" => "972",
            "rating" => "1.2",
            "sharing" => "401",
            "source" => "http://tele7jeux.fr/jeux/test-psycho/connaissez-vous-les-bonnes-manieres/203",
        ]);

        DB::table('questions')->insert([
            "created_at" => "2018-10-20",
            "updated_at" => "2018-10-20",
            "name" => "Quand arriver lorsque l'on est invité à manger chez un hôte ?",
            "explanation" => "Concept très français, le quart d’heure de politesse permet aux invités d’arriver avec ¼ d’heure pour permettre aux hôtes de fignoler les derniers préparatifs. Néanmoins, la ponctualité n’est pas mal vue.",
            "views" => "597",
            "favourites" => "105",
            "votes" => "422",
            "rating" => "1.7",
            "sharing" => "504",
            "source" => "http://tele7jeux.fr/jeux/test-psycho/connaissez-vous-les-bonnes-manieres/203",
        ]);

        DB::table('questions')->insert([
            "created_at" => "2018-11-22",
            "updated_at" => "2018-11-22",
            "name" => "Comment prendre congé de ses hôtes ?",
            "explanation" => "En théorie, on ne doit pas donner le signal du départ avant celui ou celle pour qui le dîner a été offert ou les personnes les plus âgées. Quelle que soit l’invitation, vous devez toujours remercier, même si vous êtes venu avec un présent. Par courriel ou par téléphone pour un simple verre. En revanche, si vous avez été convié à un événement un peu exceptionnel, c’est la plume qu’il vous faudra prendre pour remercier la maîtresse de maison, même s’il s’agit d’un couple.",
            "views" => "42",
            "favourites" => "355",
            "votes" => "736",
            "rating" => "1.4",
            "sharing" => "125",
            "source" => "http://www.lefigaro.fr/actualite-france/2016/12/09/01016-20161209ARTFIG00285-tes-vous-poli-testez-vos-bonnes-manieres.php",
        ]);

        DB::table('questions')->insert([
            "created_at" => "2019-02-07",
            "updated_at" => "2019-02-07",
            "name" => "Votre patron vous « ajoute » sur Facebook, que faites-vous ?",
            "explanation" => "Il ne faut pas refuser catégoriquement, c'est bien trop frontal. Le mieux que vous puissiez faire, si vous n'êtes pas sûr de vous, c'est de temporiser. Cela peut durer des mois entiers… peut-être finira-t-il par oublier l’invitation ? Mais la plupart du temps, cahin-caha, on finit bien par accepter, pour éviter tout malaise. Et après tout, vous pouvez toujours choisir dans les réglages ce que vous souhaitez montrer (ou pas) à votre supérieur hiérarchique.",
            "views" => "256",
            "favourites" => "427",
            "votes" => "422",
            "rating" => "1.9",
            "sharing" => "91",
            "source" => "http://www.lefigaro.fr/actualite-france/2016/12/09/01016-20161209ARTFIG00285-tes-vous-poli-testez-vos-bonnes-manieres.php",
        ]);

        DB::table('questions')->insert([
            "created_at" => "2019-01-13",
            "updated_at" => "2019-01-13",
            "name" => "Peut-on souhaiter « Bon courage » à un collègue ?",
            "explanation" => "Si vous vous êtes persuadé que &quot;Bon courage&quot; est une expression positive, détrompez-vous! On souhaite à quelqu'un «bon courage» avant qu'il n'affronte une tâche ingrate, un mauvais moment à passer... Dans ce cas, pourquoi souhaiter «bon courage» à un collègue que l'on croise dans l'ascenseur ou au détour d'un couloir, sans raison apparente? Cette expression sous-entend qu'une journée de travail ne peut-être que pénible. En revanche, si votre collègue se casse la jambe à deux jours de son départ en vacances, il sera légitime de lui souhaiter « Bon courage »…",
            "views" => "93",
            "favourites" => "370",
            "votes" => "607",
            "rating" => "1.8",
            "sharing" => "829",
            "source" => "http://www.lefigaro.fr/actualite-france/2016/12/09/01016-20161209ARTFIG00285-tes-vous-poli-testez-vos-bonnes-manieres.php",
        ]);

        DB::table('questions')->insert([
            "created_at" => "2019-02-02",
            "updated_at" => "2019-02-02",
            "name" => "Vous invitez des amis à dîner. A leur arrivée, ils vous offrent une bouteille de champagne.",
            "explanation" => "C’est maintenant et avec la personne qui vous l’a apportée que vous devez boire cette bouteille. C’est un peu crispant, pour l’invité, de voir son présent oublié dans un coin. Et surtout, remerciez discrètement cette personne afin de ne pas mettre mal à l’aise ceux qui sont arrivés les mains vides…",
            "views" => "770",
            "favourites" => "192",
            "votes" => "945",
            "rating" => "1.8",
            "sharing" => "338",
            "source" => "http://www.lefigaro.fr/actualite-france/2016/12/09/01016-20161209ARTFIG00285-tes-vous-poli-testez-vos-bonnes-manieres.php",
        ]);

        DB::table('questions')->insert([
            "created_at" => "2018-09-16",
            "updated_at" => "2018-09-16",
            "name" => "Quelle formulation est correcte ?",
            "explanation" => "On ne doit jamais prononcer la particule « de » lorsqu’on cite un nom de famille aristocratique sans le prénom. Mais il y a des exceptions : quand la particule est un « d’ », un « du » ou un « des » et quand le nom de famille ne comporte qu’une syllabe.",
            "views" => "299",
            "favourites" => "400",
            "votes" => "378",
            "rating" => "1.8",
            "sharing" => "771",
            "source" => "http://www.lefigaro.fr/actualite-france/2016/12/09/01016-20161209ARTFIG00285-tes-vous-poli-testez-vos-bonnes-manieres.php",
        ]);

        DB::table('questions')->insert([
            "created_at" => "2018-09-24",
            "updated_at" => "2018-09-24",
            "name" => "Au moment de rencontrer quelqu’un, que faites-vous si vous avez les mains moites ?",
            "explanation" => "Votre interlocuteur n'a probablement pas très envie d'entendre parler de votre transpiration... Quant à essuyer sa main, même discrètement, ce n’est pas chic. Alors tant pis !",
            "views" => "665",
            "favourites" => "881",
            "votes" => "362",
            "rating" => "1.2",
            "sharing" => "210",
            "source" => "http://www.lefigaro.fr/actualite-france/2016/12/09/01016-20161209ARTFIG00285-tes-vous-poli-testez-vos-bonnes-manieres.php",
        ]);


    }
}
