<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            "name" => "Galanterie",
            "icon" => "icon.jpg",
            "image" => "galanterie.svg",
        ]);

        DB::table('categories')->insert([
            "name" => "Communication",
            "icon" => "icon.jpg",
            "image" => "communication.svg",
        ]);

        DB::table('categories')->insert([
            "name" => "Conversation",
            "icon" => "icon.jpg",
            "image" => "conversation.svg",
        ]);

        DB::table('categories')->insert([
            "name" => "Apparence",
            "icon" => "icon.jpg",
            "image" => "apparence.svg",
        ]);

        DB::table('categories')->insert([
            "name" => "Vestimentaire",
            "icon" => "icon.jpg",
            "image" => "vestimentaire.svg",
        ]);

        DB::table('categories')->insert([
            "name" => "Savoir-être",
            "icon" => "icon.jpg",
            "image" => "savoir-etre.svg",
        ]);

        DB::table('categories')->insert([
            "name" => "Au restaurant",
            "icon" => "icon.jpg",
            "image" => "restaurant.svg",
        ]);

        DB::table('categories')->insert([
            "name" => "Au travail",
            "icon" => "icon.jpg",
            "image" => "travail.svg",
        ]);

        DB::table('categories')->insert([
            "name" => "Salutations",
            "icon" => "icon.jpg",
            "image" => "salutations.svg",
        ]);

        DB::table('categories')->insert([
            "name" => "À table",
            "icon" => "icon.jpg",
            "image" => "table.svg",
        ]);


    }
}
