<?php

use Illuminate\Database\Seeder;

class DifficultiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('difficulties')->insert([
            'name' => 'Easy',
            'points' => '200',
        ]);

        DB::table('difficulties')->insert([
            'name' => 'Intermediate',
            'points' => '300',
        ]);

        DB::table('difficulties')->insert([
            'name' => 'Hard',
            'points' => '500',
        ]);
    }
}
