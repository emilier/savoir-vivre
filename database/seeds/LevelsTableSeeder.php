<?php

use Illuminate\Database\Seeder;

class LevelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('levels')->insert([
            'level' => '1',
            'name' => 'Roturier',
            'female_name' => 'Roturière',
            'score' => '100000',
            'emoji' => 'sv-neutral.svg',
        ]);

        DB::table('levels')->insert([
            'level' => '2',
            'name' => 'Écuyer',
            'female_name' => 'Damoiseau',
            'title' => 'Messire',
            'female_title' => 'Dame',
            'score' => '200000',
            'emoji' => 'sv-neutral.svg',
            'thumb' => 'thumb-shield.svg',
            'background' => 'shield.svg',
        ]);

        DB::table('levels')->insert([
            'level' => '3',
            'name' => 'Chevalier',
            'female_name' => 'Dame',
            'title' => 'Messire',
            'female_title' => 'Dame',
            'score' => '300000',
            'emoji' => 'sv-neutral.svg',
            'thumb' => 'sword.svg',
            'background' => 'shield.svg',
        ]);

//        DB::table('levels')->insert([
//            'name' => 'Seigneur',
//            'female_name' => 'Seigneuresse',
//            'title' => 'Messire',
//            'female_title' => 'Dame',
//            'score' => '',
//            'thumb' => 'level.jpg',
//        ]);

//        DB::table('levels')->insert([
//            'name' => 'Banneret',
//            'female_name' => 'Banneret',
//            'score' => '',
//            'thumb' => 'level.jpg',
//        ]);

        DB::table('levels')->insert([
            'level' => '4',
            'name' => 'Baron',
            'female_name' => 'Baronne',
            'title' => 'Messire',
            'female_title' => 'Dame',
            'emoji' => 'sv-neutral.svg',
            'score' => '400000',
            'thumb' => 'level.jpg',
        ]);

//        DB::table('levels')->insert([
//            'name' => 'Vicomte',
//            'female_name' => 'Vicomtesse',
//            'title' => 'Messire',
//            'female_title' => 'Dame',
//            'score' => '',
//            'thumb' => 'level.jpg',
//        ]);

//        DB::table('levels')->insert([
//            'name' => 'Vidame',
//            'female_name' => 'Comtesse',
//            'title' => 'Mon seigneur',
//            'female_title' => 'Dame',
//            'score' => '',
//            'thumb' => 'level.jpg',
//        ]);

        DB::table('levels')->insert([
            'level' => '5',
            'name' => 'Comte',
            'female_name' => 'Comtesse',
            'title' => 'Mon seigneur',
            'female_title' => 'Dame',
            'emoji' => 'sv-neutral.svg',
            'score' => '500000',
            'thumb' => 'level.jpg',
        ]);

        DB::table('levels')->insert([
            'level' => '6',
            'name' => 'Marquis',
            'female_name' => 'Marquise',
            'title' => 'Votre grandeur',
            'female_title' => 'Votre grandeur',
            'emoji' => 'sv-neutral.svg',
            'score' => '600000',
            'thumb' => 'level.jpg',
        ]);

        DB::table('levels')->insert([
            'level' => '7',
            'name' => 'Duc',
            'female_name' => 'Duchesse',
            'title' => 'Votre grandeur',
            'female_title' => 'Votre grandeur',
            'emoji' => 'sv-neutral.svg',
            'score' => '700000',
            'thumb' => 'level.jpg',
        ]);

        DB::table('levels')->insert([
            'level' => '8',
            'name' => 'Prince',
            'female_name' => 'Princesse',
            'title' => 'Votre altesse',
            'female_title' => 'Votre altesse',
            'emoji' => 'sv-neutral.svg',
            'score' => '800000',
            'thumb' => 'level.jpg',
        ]);

        DB::table('levels')->insert([
            'level' => '9',
            'name' => 'Roi',
            'female_name' => 'Renne',
            'title' => 'Votre majesté',
            'female_title' => 'Votre majesté',
            'emoji' => 'sv-neutral.svg',
            'score' => '900000',
            'thumb' => 'level.jpg',
        ]);

        DB::table('levels')->insert([
            'level' => '10',
            'name' => 'Empereur',
            'female_name' => 'Impératrice',
            'title' => 'Votre majesté',
            'female_title' => 'Votre majesté',
            'emoji' => 'sv-neutral.svg',
            'score' => '9999999',
            'thumb' => 'level.jpg',
        ]);
    }
}
