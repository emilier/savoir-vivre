<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        for($i = 0; $i < 10; ++$i) {
//            DB::table('users')->insert([
//                'pseudo' => 'Name' . $i,
//                'email' => 'email' . $i . '@gmail.com',
//                'password' => bcrypt('password' . $i),
//                'gender' => 'male',
//            ]);
//        }

//        factory(App\Models\User::class, 50)->create()->each(function ($user) {
//            $user->posts()->save(factory(App\Post::class)->make());
//        });

        DB::table('users')->insert([
            'pseudo' => 'Marie Stuart',
            'email' => 'emilie.40@hotmail.com',
            'password' => bcrypt('Milou40SavoirVivre'),
            'gender' => 'female',
        ]);

        factory(App\Models\User::class, 20)->create();
    }
}
