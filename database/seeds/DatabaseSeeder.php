<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CountriesTableSeeder::class);
        $this->call(RulersTableSeeder::class);
        $this->call(LevelsTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(DifficultiesTableSeeder::class);
        $this->call(QuestionsTableSeeder::class);
        $this->call(CategoryQuestionTableSeeder::class);
        $this->call(QuestionUserTableSeeder::class);
        $this->call(AnswersTableSeeder::class);
    }
}
