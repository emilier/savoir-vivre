<?php

use Illuminate\Database\Seeder;

class AnswersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        DB::table('answers')->insert([
//            'name' => 'À côté d\'elle du côté du bord du trottoir',
//            'question_id' => '﻿1',
//            'correct' => '1',
//        ]);

//        DB::table('answers')->insert([
//            'name' => 'À côté d\'elle près des murs',
//            'question_id' => '﻿1',
//            'correct' => '0',
//        ]);
//
//        DB::table('answers')->insert([
//            'name' => '10 mètres devant elle',
//            'question_id' => '﻿1',
//            'correct' => '0',
//        ]);
//
//        DB::table('answers')->insert([
//            'name' => 'Juste derrière elle',
//            'question_id' => '﻿1',
//            'correct' => '0',
//        ]);

        DB::table('answers')->insert([
            'name' => 'Devant, à côté du conducteur',
            'question_id' => '2',
            'correct' => '1',
        ]);

        DB::table('answers')->insert([
            'name' => 'Derrière, à droite',
            'question_id' => '2',
            'correct' => '0',
        ]);

        DB::table('answers')->insert([
            'name' => 'Derrière, à gauche',
            'question_id' => '2',
            'correct' => '0',
        ]);

        DB::table('answers')->insert([
            'name' => 'L\'homme passe toujours devant',
            'question_id' => '3',
            'correct' => '1',
        ]);

        DB::table('answers')->insert([
            'name' => 'L\'homme est toujours derrière',
            'question_id' => '3',
            'correct' => '0',
        ]);

        DB::table('answers')->insert([
            'name' => 'L\'homme est devant uniquement pour descendre',
            'question_id' => '3',
            'correct' => '0',
        ]);

        DB::table('answers')->insert([
            'name' => 'L\'homme est devant uniquement pour monter',
            'question_id' => '3',
            'correct' => '0',
        ]);

        DB::table('answers')->insert([
            'name' => 'Vrai',
            'question_id' => '4',
            'correct' => '1',
        ]);

        DB::table('answers')->insert([
            'name' => 'Faux',
            'question_id' => '4',
            'correct' => '0',
        ]);

        DB::table('answers')->insert([
            'name' => 'Faux',
            'question_id' => '5',
            'correct' => '1',
        ]);

        DB::table('answers')->insert([
            'name' => 'Vrai',
            'question_id' => '5',
            'correct' => '0',
        ]);

        DB::table('answers')->insert([
            'name' => '7h45',
            'question_id' => '6',
            'correct' => '1',
        ]);

        DB::table('answers')->insert([
            'name' => '8h tapantes',
            'question_id' => '6',
            'correct' => '0',
        ]);

        DB::table('answers')->insert([
            'name' => '7h55',
            'question_id' => '6',
            'correct' => '0',
        ]);

        DB::table('answers')->insert([
            'name' => 'Vous acceptez spontanément et vous vous mettez en route tout de suite',
            'question_id' => '7',
            'correct' => '1',
        ]);

        DB::table('answers')->insert([
            'name' => 'Vous posez vos conditions : en échange, vous exigez des pauses plus longues chaque midi',
            'question_id' => '7',
            'correct' => '0',
        ]);

        DB::table('answers')->insert([
            'name' => 'Vous refusez : c’est votre jour de repos, point barre !',
            'question_id' => '7',
            'correct' => '0',
        ]);

        DB::table('answers')->insert([
            'name' => 'Vous lui proposez un coup de main, en faisant les photocopies dont il a besoin ou en l’aidant à ranger son rayon',
            'question_id' => '8',
            'correct' => '1',
        ]);

        DB::table('answers')->insert([
            'name' => 'C’est son problème : vous, vous avez bien mérité une petite pause !',
            'question_id' => '8',
            'correct' => '0',
        ]);

        DB::table('answers')->insert([
            'name' => 'Vous lui faites remarquer qu’il est nul et vous vous moquez de lui',
            'question_id' => '8',
            'correct' => '0',
        ]);

        DB::table('answers')->insert([
            'name' => 'Dès que vous avez identifié de quoi il s’agit, vous allez trouver Nicole pour lui rendre discrètement ce document confidentiel',
            'question_id' => '9',
            'correct' => '1',
        ]);

        DB::table('answers')->insert([
            'name' => 'Vous vous enfermez dans votre bureau pour le lire de la première à la dernière ligne : certaines informations pourraient un jour vous être utiles…',
            'question_id' => '9',
            'correct' => '0',
        ]);

        DB::table('answers')->insert([
            'name' => 'Sous la rubrique « Salaire », vous découvrez qu’elle gagne plus que vous : vous ameutez tous vos collègues et hurlez que c’est un scandale',
            'question_id' => '9',
            'correct' => '0',
        ]);

        DB::table('answers')->insert([
            'name' => 'Vous l’écoutez attentivement, puis reformulez sa plainte avant de lui apporter des éléments de solution avec un langage positif',
            'question_id' => '10',
            'correct' => '1',
        ]);

        DB::table('answers')->insert([
            'name' => 'Vous le prévenez que vous êtes ceinture noire de karaté',
            'question_id' => '10',
            'correct' => '0',
        ]);

        DB::table('answers')->insert([
            'name' => 'Vous l’interrompez et lui demandez de se calmer',
            'question_id' => '10',
            'correct' => '0',
        ]);

        DB::table('answers')->insert([
            'name' => '« J’aime bien ce que je fais, et je me vois bien progresser dans ce métier. »',
            'question_id' => '11',
            'correct' => '1',
        ]);

        DB::table('answers')->insert([
            'name' => '« Le salaire, bien sûr ! »',
            'question_id' => '11',
            'correct' => '0',
        ]);

        DB::table('answers')->insert([
            'name' => '« J’habite juste à côté : c’est pratique. »',
            'question_id' => '11',
            'correct' => '0',
        ]);

        DB::table('answers')->insert([
            'name' => 'Vous assumez votre erreur et allez en informer votre supérieur en lui proposant une solution pour la rectifier',
            'question_id' => '12',
            'correct' => '1',
        ]);

        DB::table('answers')->insert([
            'name' => 'Vous mettez l’erreur sur le compte du secrétariat',
            'question_id' => '12',
            'correct' => '0',
        ]);

        DB::table('answers')->insert([
            'name' => 'Vous ne dites rien : pas la peine de s’attirer des ennuis, il sera toujours temps d’aviser quand Duschmoll s’en apercevra',
            'question_id' => '12',
            'correct' => '0',
        ]);

        DB::table('answers')->insert([
            'name' => 'Vous faites part à Georgette de vos projets pour le déjeuner et lui proposez de se joindre au groupe',
            'question_id' => '13',
            'correct' => '1',
        ]);

        DB::table('answers')->insert([
            'name' => 'Vous prenez vos affaires sans rien dire et allez rejoindre vos copines du service informatique à la pizzeria voisine',
            'question_id' => '13',
            'correct' => '0',
        ]);

        DB::table('answers')->insert([
            'name' => 'Avant de quitter le bureau, vous indiquez à Georgette qu’il y a un camion-bar au coin de la rue',
            'question_id' => '13',
            'correct' => '0',
        ]);

        DB::table('answers')->insert([
            'name' => 'Oui, c\'est un signe de respect',
            'question_id' => '14',
            'correct' => '1',
        ]);

        DB::table('answers')->insert([
            'name' => 'Non, c\'est un manque de respect',
            'question_id' => '14',
            'correct' => '0',
        ]);

        DB::table('answers')->insert([
            'name' => 'Je ne parle ni de moi, ni de l\'autre',
            'question_id' => '15',
            'correct' => '1',
        ]);

        DB::table('answers')->insert([
            'name' => 'Je parle de la météo et du beau temps',
            'question_id' => '15',
            'correct' => '0',
        ]);

        DB::table('answers')->insert([
            'name' => 'Je parle de moi',
            'question_id' => '15',
            'correct' => '0',
        ]);

        DB::table('answers')->insert([
            'name' => 'On parle de l\'autre',
            'question_id' => '15',
            'correct' => '0',
        ]);

        DB::table('answers')->insert([
            'name' => 'M.',
            'question_id' => '16',
            'correct' => '1',
        ]);

        DB::table('answers')->insert([
            'name' => 'Mr',
            'question_id' => '16',
            'correct' => '0',
        ]);

        DB::table('answers')->insert([
            'name' => 'Oui, uniquement aux personnes qui je connais',
            'question_id' => '17',
            'correct' => '1',
        ]);

        DB::table('answers')->insert([
            'name' => 'J\'attends de ressentir la situation et de découvrir mon interlocuteur',
            'question_id' => '18',
            'correct' => '1',
        ]);

        DB::table('answers')->insert([
            'name' => 'Je dis &quot;tu&quot;',
            'question_id' => '18',
            'correct' => '0',
        ]);

        DB::table('answers')->insert([
            'name' => 'Je dis &quot;vous&quot;',
            'question_id' => '18',
            'correct' => '0',
        ]);

        DB::table('answers')->insert([
            'name' => 'La plus jeune à la plus âgée',
            'question_id' => '19',
            'correct' => '1',
        ]);

        DB::table('answers')->insert([
            'name' => 'La femme à l’homme',
            'question_id' => '19',
            'correct' => '0',
        ]);

        DB::table('answers')->insert([
            'name' => 'La moins jeune à la plus âgée',
            'question_id' => '19',
            'correct' => '0',
        ]);

        DB::table('answers')->insert([
            'name' => 'Eau, vin rouge, vin blanc',
            'question_id' => '20',
            'correct' => '1',
        ]);

        DB::table('answers')->insert([
            'name' => 'Vin rouge, vin blanc, eau',
            'question_id' => '20',
            'correct' => '0',
        ]);

        DB::table('answers')->insert([
            'name' => 'Vin blanc, vin rouge, eau',
            'question_id' => '20',
            'correct' => '0',
        ]);

        DB::table('answers')->insert([
            'name' => 'De l´extérieur vers l´intérieur',
            'question_id' => '21',
            'correct' => '1',
        ]);

        DB::table('answers')->insert([
            'name' => 'De l´intérieur vers l´extérieur',
            'question_id' => '21',
            'correct' => '0',
        ]);

        DB::table('answers')->insert([
            'name' => 'Au pif... au feeling',
            'question_id' => '21',
            'correct' => '0',
        ]);

        DB::table('answers')->insert([
            'name' => 'Le vinaigre corrode les couteaux',
            'question_id' => '22',
            'correct' => '1',
        ]);

        DB::table('answers')->insert([
            'name' => 'Parce que !... Il ne faut pas !',
            'question_id' => '22',
            'correct' => '0',
        ]);

        DB::table('answers')->insert([
            'name' => 'Les couteaux sont enlevés à ce moment-là',
            'question_id' => '22',
            'correct' => '0',
        ]);

        DB::table('answers')->insert([
            'name' => 'Dans l’assiette alignés',
            'question_id' => '23',
            'correct' => '1',
        ]);

        DB::table('answers')->insert([
            'name' => 'De chaque côté de l’assiette',
            'question_id' => '23',
            'correct' => '0',
        ]);

        DB::table('answers')->insert([
            'name' => 'D’un seul côté de l’assiette',
            'question_id' => '23',
            'correct' => '0',
        ]);

        DB::table('answers')->insert([
            'name' => 'Dans l\'assiette en croix',
            'question_id' => '23',
            'correct' => '0',
        ]);

        DB::table('answers')->insert([
            'name' => 'Ravi(e) de vous rencontrer',
            'question_id' => '24',
            'correct' => '1',
        ]);

        DB::table('answers')->insert([
            'name' => 'Enchanté(s)',
            'question_id' => '24',
            'correct' => '0',
        ]);

        DB::table('answers')->insert([
            'name' => 'Salut',
            'question_id' => '24',
            'correct' => '0',
        ]);

        DB::table('answers')->insert([
            'name' => 'La personne qui appelle',
            'question_id' => '25',
            'correct' => '1',
        ]);

        DB::table('answers')->insert([
            'name' => 'L\'une ou l\'autre, peu importe',
            'question_id' => '25',
            'correct' => '0',
        ]);

        DB::table('answers')->insert([
            'name' => 'La personne qui reçoit l’appel',
            'question_id' => '25',
            'correct' => '0',
        ]);

        DB::table('answers')->insert([
            'name' => 'La salade, les pâtes et l’omelette',
            'question_id' => '26',
            'correct' => '1',
        ]);

        DB::table('answers')->insert([
            'name' => 'Les frites, la viande hâchée et les carottes',
            'question_id' => '26',
            'correct' => '0',
        ]);

        DB::table('answers')->insert([
            'name' => 'Le poisson, les légumes cuits et le gâteau',
            'question_id' => '26',
            'correct' => '0',
        ]);

        DB::table('answers')->insert([
            'name' => '20h',
            'question_id' => '27',
            'correct' => '1',
        ]);

        DB::table('answers')->insert([
            'name' => '21h',
            'question_id' => '27',
            'correct' => '0',
        ]);

        DB::table('answers')->insert([
            'name' => '22h',
            'question_id' => '27',
            'correct' => '0',
        ]);

        DB::table('answers')->insert([
            'name' => 'L’homme entre devant la femme',
            'question_id' => '28',
            'correct' => '1',
        ]);

        DB::table('answers')->insert([
            'name' => 'L’homme et la femme rentrent en même temps',
            'question_id' => '28',
            'correct' => '0',
        ]);

        DB::table('answers')->insert([
            'name' => 'La femme rentre la première',
            'question_id' => '28',
            'correct' => '0',
        ]);

        DB::table('answers')->insert([
            'name' => 'Lorsque l\'hôte de maison a donné le premier coup de fourchette',
            'question_id' => '29',
            'correct' => '1',
        ]);

        DB::table('answers')->insert([
            'name' => 'Après avoir souhaité « bon appétit »',
            'question_id' => '29',
            'correct' => '0',
        ]);

        DB::table('answers')->insert([
            'name' => 'Dès que le mets est servi',
            'question_id' => '29',
            'correct' => '0',
        ]);

        DB::table('answers')->insert([
            'name' => 'Avec ¼ d’heure de retard',
            'question_id' => '30',
            'correct' => '1',
        ]);

        DB::table('answers')->insert([
            'name' => 'Avec un ¼ d’heure en avance',
            'question_id' => '30',
            'correct' => '0',
        ]);

        DB::table('answers')->insert([
            'name' => 'Pile à l\'heure',
            'question_id' => '30',
            'correct' => '0',
        ]);

        DB::table('answers')->insert([
            'name' => 'Ne partez jamais avant l’invité d’honneur ou le plus âgé ou le plus important',
            'question_id' => '31',
            'correct' => '1',
        ]);

        DB::table('answers')->insert([
            'name' => 'Partez le dernier si vous vous sentez bien et que vous voulez le montrer à vos hôtes',
            'question_id' => '31',
            'correct' => '0',
        ]);

        DB::table('answers')->insert([
            'name' => 'Partez dès que vous en avez envie',
            'question_id' => '31',
            'correct' => '0',
        ]);

        DB::table('answers')->insert([
            'name' => 'Vous acceptez',
            'question_id' => '32',
            'correct' => '1',
        ]);

        DB::table('answers')->insert([
            'name' => 'Vous refusez',
            'question_id' => '32',
            'correct' => '0',
        ]);

        DB::table('answers')->insert([
            'name' => 'Vous temporisez',
            'question_id' => '32',
            'correct' => '0',
        ]);

        DB::table('answers')->insert([
            'name' => 'Oui, bien sûr',
            'question_id' => '33',
            'correct' => '1',
        ]);

        DB::table('answers')->insert([
            'name' => 'Non, jamais',
            'question_id' => '33',
            'correct' => '0',
        ]);

        DB::table('answers')->insert([
            'name' => 'À certaines conditions',
            'question_id' => '33',
            'correct' => '0',
        ]);

        DB::table('answers')->insert([
            'name' => 'Vous servez immédiatement ce nouveau champagne',
            'question_id' => '34',
            'correct' => '1',
        ]);

        DB::table('answers')->insert([
            'name' => 'Vous la rangez aussitôt à la cave en position allongée',
            'question_id' => '34',
            'correct' => '0',
        ]);

        DB::table('answers')->insert([
            'name' => 'Vous la posez bien en vue mais ouvrez celle que vous aviez prévu de servir',
            'question_id' => '34',
            'correct' => '0',
        ]);

        DB::table('answers')->insert([
            'name' => 'J’ai invité à dîner les Vilmorin',
            'question_id' => '35',
            'correct' => '1',
        ]);

        DB::table('answers')->insert([
            'name' => 'J’ai invité à dîner les de Vilmorin',
            'question_id' => '35',
            'correct' => '0',
        ]);

        DB::table('answers')->insert([
            'name' => 'J’ai invité à dîner la famille de Vilmorin',
            'question_id' => '35',
            'correct' => '0',
        ]);

        DB::table('answers')->insert([
            'name' => 'Vous serrez la main de votre interlocuteur en souriant',
            'question_id' => '36',
            'correct' => '1',
        ]);

        DB::table('answers')->insert([
            'name' => 'Vous essuyez discrètement votre main sur votre pantalon avant de la tendre à cette personne',
            'question_id' => '36',
            'correct' => '0',
        ]);

        DB::table('answers')->insert([
            'name' => 'Vous lui serrez la main mais vous vous excusez pour ce petit désagrément',
            'question_id' => '36',
            'correct' => '0',
        ]);
    }
}
