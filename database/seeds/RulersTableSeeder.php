<?php

use Illuminate\Database\Seeder;

class RulersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('rulers')->insert([
            'name' => 'Pépin le Bref',
            'gender' => 'male'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Carloman Ier',
            'gender' => 'male'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Charlemagne',
            'gender' => 'male'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Louis Ier le Pieux',
            'gender' => 'male'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Charles II le Chauve',
            'gender' => 'male'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Louis II le Bègue',
            'gender' => 'male'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Louis III',
            'gender' => 'male'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Carloman II',
            'gender' => 'male'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Charles III le Gros',
            'gender' => 'male'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Eudes Ier',
            'gender' => 'male'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Charles III le Simple',
            'gender' => 'male'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Robert Ier',
            'gender' => 'male'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Raoul Ier',
            'gender' => 'male'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Louis IV d\'Outremer',
            'gender' => 'male'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Lothaire',
            'gender' => 'male'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Louis V le Fainéant',
            'gender' => 'male'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Hugues Capet',
            'gender' => 'male'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Robert II le Pieux',
            'gender' => 'male'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Henri Ier',
            'gender' => 'male'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Philippe Ier',
            'gender' => 'male'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Louis VI le Gros',
            'gender' => 'male'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Louis VII le Jeune',
            'gender' => 'male'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Philippe II Auguste',
            'gender' => 'male'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Louis VIII le Lion',
            'gender' => 'male'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Louis IX ou Saint-Louis',
            'gender' => 'male'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Philippe III le Hardi',
            'gender' => 'male'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Philippe IV le Bel',
            'gender' => 'male'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Louis X le Hutin',
            'gender' => 'male'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Jean Ier le Posthume',
            'gender' => 'male'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Philippe V le Long',
            'gender' => 'male'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Charles IV le Bel',
            'gender' => 'male'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Philippe VI de Valois',
            'gender' => 'male'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Jean II le Bon',
            'gender' => 'male'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Charles V le Sage',
            'gender' => 'male'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Charles VI le Fou',
            'gender' => 'male'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Charles VII le Victorieux',
            'gender' => 'male'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Louis XI de France',
            'gender' => 'male'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Charles VIII l\'Affable',
            'gender' => 'male'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Louis XII',
            'gender' => 'male'
        ]);
        DB::table('rulers')->insert([
            'name' => 'François Ier',
            'gender' => 'male'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Henri II',
            'gender' => 'male'
        ]);
        DB::table('rulers')->insert([
            'name' => 'François II',
            'gender' => 'male'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Charles IX',
            'gender' => 'male'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Henri III',
            'gender' => 'male'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Henri IV le Grand',
            'gender' => 'male'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Louis XIII le Juste',
            'gender' => 'male'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Louis XIV le Roi Soleil',
            'gender' => 'male'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Louis XV',
            'gender' => 'male'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Louis XVI',
            'gender' => 'male'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Napoléon Ier',
            'gender' => 'male'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Napoléon II',
            'gender' => 'male'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Louis XVIII',
            'gender' => 'male'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Charles X',
            'gender' => 'male'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Louis-Philippe Ier',
            'gender' => 'male'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Napoléon III',
            'gender' => 'male'
        ]);



        DB::table('rulers')->insert([
            'name' => 'Berthe de Laon au Grand Pied',
            'gender' => 'female'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Himiltrude',
            'gender' => 'female'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Désirée de Lombardie',
            'gender' => 'female'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Hildegarde de Vintzgau',
            'gender' => 'female'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Fastrade de Franconie',
            'gender' => 'female'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Luitgarde d\'Alémanie',
            'gender' => 'female'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Madelgrade',
            'gender' => 'female'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Gerswinde de Saxe',
            'gender' => 'female'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Ermengarde de Hesbaye',
            'gender' => 'female'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Judith de Bavière',
            'gender' => 'female'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Ermentrude d\'Orléans',
            'gender' => 'female'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Richilde de Provence',
            'gender' => 'female'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Ansgarde d\'Hiémois',
            'gender' => 'female'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Adélaïde de Frioul',
            'gender' => 'female'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Richarde de Souabe',
            'gender' => 'female'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Théodérade',
            'gender' => 'female'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Frédérune',
            'gender' => 'female'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Edwige de Wessex',
            'gender' => 'female'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Béatrice de Vermandois',
            'gender' => 'female'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Emma de France',
            'gender' => 'female'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Gerberge de Saxe',
            'gender' => 'female'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Emma d\'Italie',
            'gender' => 'female'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Blanche',
            'gender' => 'female'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Adélaïde d\'Aquitaine',
            'gender' => 'female'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Rozala d\'Italie',
            'gender' => 'female'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Berthe de Bourgogne',
            'gender' => 'female'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Constance d\'Arles',
            'gender' => 'female'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Mathilde de Frise',
            'gender' => 'female'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Anne de Kiev',
            'gender' => 'female'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Berthe de Hollande',
            'gender' => 'female'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Bertrade de Montfort',
            'gender' => 'female'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Adélaïde de Savoie',
            'gender' => 'female'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Aliénor d\'Aquitaine',
            'gender' => 'female'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Constance de Castille',
            'gender' => 'female'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Adèle de Champagne',
            'gender' => 'female'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Isabelle de Hainaut',
            'gender' => 'female'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Ingeburge de Danemark',
            'gender' => 'female'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Agnès de Méranie',
            'gender' => 'female'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Blanche de Castille',
            'gender' => 'female'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Marguerite de Provence',
            'gender' => 'female'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Isabelle d\'Aragon',
            'gender' => 'female'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Marie de Brabant',
            'gender' => 'female'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Jeanne Ire de Navarre',
            'gender' => 'female'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Marguerite de Bourgogne',
            'gender' => 'female'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Clémence de Hongrie',
            'gender' => 'female'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Jeanne II de Bourgogne',
            'gender' => 'female'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Blanche de Bourgogne',
            'gender' => 'female'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Marie de Luxembourg',
            'gender' => 'female'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Jeanne d\'Évreux',
            'gender' => 'female'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Jeanne de Bourgogne',
            'gender' => 'female'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Blanche de Navarre',
            'gender' => 'female'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Jeanne d\'Auvergne',
            'gender' => 'female'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Jeanne de Bourbon',
            'gender' => 'female'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Isabeau de Bavière',
            'gender' => 'female'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Marie d\'Anjou',
            'gender' => 'female'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Marguerite d\'Écosse',
            'gender' => 'female'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Charlotte de Savoie',
            'gender' => 'female'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Anne de Bretagne',
            'gender' => 'female'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Marguerite d\'Autriche',
            'gender' => 'female'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Jeanne de France',
            'gender' => 'female'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Marie d\'Angleterre',
            'gender' => 'female'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Claude de France',
            'gender' => 'female'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Éléonore d\'Autriche',
            'gender' => 'female'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Catherine de Médicis',
            'gender' => 'female'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Marie Stuart',
            'gender' => 'female'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Élisabeth d\'Autriche',
            'gender' => 'female'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Louise de Lorraine-Vaudémont',
            'gender' => 'female'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Marguerite de France',
            'gender' => 'female'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Marie de Médicis',
            'gender' => 'female'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Anne d\'Autriche',
            'gender' => 'female'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Marie-Thérèse d\'Autriche',
            'gender' => 'female'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Marie Leszczyńska',
            'gender' => 'female'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Marie-Antoinette d\'Autriche',
            'gender' => 'female'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Joséphine de Beauharnais',
            'gender' => 'female'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Marie-Louise d\'Autriche',
            'gender' => 'female'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Marie-Joséphine de Savoie',
            'gender' => 'female'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Marie-Thérèse de Savoie',
            'gender' => 'female'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Marie-Amélie des Deux-Siciles',
            'gender' => 'female'
        ]);
        DB::table('rulers')->insert([
            'name' => 'Eugénie de Montijo',
            'gender' => 'female'
        ]);
    }
}
