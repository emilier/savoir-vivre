<?php

use Illuminate\Database\Seeder;

class QuestionUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('question_user')->insert([
            'user_id' => '1',
            'question_id' => '3',
            'viewed' => true,
            'favourite' => true,
            'rating' => '4',
            'shared' => false,
        ]);
    }
}
