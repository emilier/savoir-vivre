<?php

use Illuminate\Database\Seeder;

class CategoryQuestionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('category_question')->insert([
            "question_id" => "1",
            "category_id" => "1",
        ]);

        DB::table('category_question')->insert([
            "question_id" => "2",
            "category_id" => "1",
        ]);

        DB::table('category_question')->insert([
            "question_id" => "3",
            "category_id" => "1",
        ]);

        DB::table('category_question')->insert([
            "question_id" => "4",
            "category_id" => "1",
        ]);

        DB::table('category_question')->insert([
            "question_id" => "5",
            "category_id" => "1",
        ]);

        DB::table('category_question')->insert([
            "question_id" => "6",
            "category_id" => "8",
        ]);

        DB::table('category_question')->insert([
            "question_id" => "7",
            "category_id" => "8",
        ]);

        DB::table('category_question')->insert([
            "question_id" => "8",
            "category_id" => "8",
        ]);

        DB::table('category_question')->insert([
            "question_id" => "9",
            "category_id" => "8",
        ]);

        DB::table('category_question')->insert([
            "question_id" => "10",
            "category_id" => "8",
        ]);

        DB::table('category_question')->insert([
            "question_id" => "11",
            "category_id" => "8",
        ]);

        DB::table('category_question')->insert([
            "question_id" => "12",
            "category_id" => "8",
        ]);

        DB::table('category_question')->insert([
            "question_id" => "13",
            "category_id" => "8",
        ]);

        DB::table('category_question')->insert([
            "question_id" => "14",
            "category_id" => "6",
        ]);

        DB::table('category_question')->insert([
            "question_id" => "14",
            "category_id" => "2",
        ]);

        DB::table('category_question')->insert([
            "question_id" => "15",
            "category_id" => "2",
        ]);

        DB::table('category_question')->insert([
            "question_id" => "15",
            "category_id" => "3",
        ]);

        DB::table('category_question')->insert([
            "question_id" => "16",
            "category_id" => "2",
        ]);

        DB::table('category_question')->insert([
            "question_id" => "17",
            "category_id" => "2",
        ]);

        DB::table('category_question')->insert([
            "question_id" => "18",
            "category_id" => "2",
        ]);

        DB::table('category_question')->insert([
            "question_id" => "19",
            "category_id" => "2",
        ]);

        DB::table('category_question')->insert([
            "question_id" => "19",
            "category_id" => "9",
        ]);

        DB::table('category_question')->insert([
            "question_id" => "20",
            "category_id" => "7",
        ]);

        DB::table('category_question')->insert([
            "question_id" => "20",
            "category_id" => "10",
        ]);

        DB::table('category_question')->insert([
            "question_id" => "21",
            "category_id" => "7",
        ]);

        DB::table('category_question')->insert([
            "question_id" => "22",
            "category_id" => "7",
        ]);

        DB::table('category_question')->insert([
            "question_id" => "23",
            "category_id" => "7",
        ]);

        DB::table('category_question')->insert([
            "question_id" => "24",
            "category_id" => "2",
        ]);

        DB::table('category_question')->insert([
            "question_id" => "24",
            "category_id" => "9",
        ]);

        DB::table('category_question')->insert([
            "question_id" => "25",
            "category_id" => "2",
        ]);

        DB::table('category_question')->insert([
            "question_id" => "25",
            "category_id" => "3",
        ]);

        DB::table('category_question')->insert([
            "question_id" => "26",
            "category_id" => "7",
        ]);

        DB::table('category_question')->insert([
            "question_id" => "27",
            "category_id" => "2",
        ]);

        DB::table('category_question')->insert([
            "question_id" => "28",
            "category_id" => "7",
        ]);

        DB::table('category_question')->insert([
            "question_id" => "29",
            "category_id" => "10",
        ]);

        DB::table('category_question')->insert([
            "question_id" => "30",
            "category_id" => "10",
        ]);

        DB::table('category_question')->insert([
            "question_id" => "31",
            "category_id" => "10",
        ]);

        DB::table('category_question')->insert([
            "question_id" => "32",
            "category_id" => "8",
        ]);

        DB::table('category_question')->insert([
            "question_id" => "32",
            "category_id" => "2",
        ]);

        DB::table('category_question')->insert([
            "question_id" => "33",
            "category_id" => "8",
        ]);

        DB::table('category_question')->insert([
            "question_id" => "33",
            "category_id" => "2",
        ]);

        DB::table('category_question')->insert([
            "question_id" => "34",
            "category_id" => "10",
        ]);

        DB::table('category_question')->insert([
            "question_id" => "35",
            "category_id" => "2",
        ]);

        DB::table('category_question')->insert([
            "question_id" => "36",
            "category_id" => "2",
        ]);

        DB::table('category_question')->insert([
            "question_id" => "36",
            "category_id" => "6",
        ]);
    }
}
