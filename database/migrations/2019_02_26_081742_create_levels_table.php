<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLevelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('levels', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';

            $table->increments('id');
            $table->nullableTimestamps();
            $table->unsignedTinyInteger('level');
            $table->string('name', 75)->unique();
            $table->string('female_name', 75)->unique();
            $table->string('title', 75)->nullable();
            $table->string('female_title', 75)->nullable();
            $table->unsignedInteger('score');
            $table->string('emoji')->nullable()->default(null);
            $table->string('thumb')->nullable()->default(null);
            $table->string('background')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('levels');
    }
}
