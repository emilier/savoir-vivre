<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';

            $table->increments('id');
            $table->nullableTimestamps();
            $table->string('pseudo', 75);
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->enum('gender', ['male', 'female']);
            $table->unsignedInteger('level_id')->default(1);
            $table->unsignedInteger('score')->default(0);
            $table->boolean('sounds')->default(true);
            $table->boolean('notifications')->default(true);
            $table->unsignedInteger('country_id')->default(76);
            $table->rememberToken();

            $table->foreign('level_id')
                ->references('id')->on('levels')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('country_id')
                ->references('id')->on('countries')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
