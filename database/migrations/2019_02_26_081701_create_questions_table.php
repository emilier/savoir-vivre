<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';

            $table->increments('id');
            $table->nullableTimestamps();
            $table->string('name')->unique();
            $table->text('explanation', 200);
            $table->boolean('rule')->default(true);
            $table->unsignedInteger('difficulty_id')->default(1);
            $table->unsignedInteger('views')->default(0);
            $table->unsignedInteger('favourites')->default(0);
            $table->unsignedInteger('votes')->default(0);
            $table->float('rating', 2, 1)->nullable()->default(null);
            $table->unsignedInteger('sharing')->default(0);
            $table->text('source', 100);
            $table->boolean('quiz')->default(true);

            $table->foreign('difficulty_id')
                ->references('id')->on('difficulties')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}
