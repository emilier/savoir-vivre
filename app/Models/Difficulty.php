<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Difficulty extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'difficulties';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'points',
    ];

    /**
     * Get the questions for the difficulty
     */
    public function questions()
    {
        return $this->hasMany('App\Models\Question');
    }
}
