<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuestionUser extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'question_user';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'question_id',
        'favourite',
        'viewed',
        'rating',
        'shared',
    ];

    /**
     * Get the user that owns the question.
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    /**
     * Get the question that owns the user.
     */
    public function question()
    {
        return $this->belongsTo('App\Models\Question');
    }
}
