<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Level extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'levels';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'level',
        'name',
        'female_name',
        'title',
        'female_title',
        'score',
        'emoji',
        'thumb',
        'background',
    ];

    /**
     * Get the users for the level.
     */
    public function users()
    {
        return $this->hasMany('App\Models\User');
    }
}
