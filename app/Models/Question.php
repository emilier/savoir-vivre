<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'questions';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'explanation',
        'rule',
        'difficulty_id',
        'views',
        'favourites',
        'votes',
        'rating',
        'sharing',
        'source',
        'quiz',
    ];

    /**
     * Get the difficulty that owns the question.
     */
    public function difficulty()
    {
        return $this->belongsTo('App\Models\Difficulty');
    }

    /**
     * Get the answers for the question.
     */
    public function answers()
    {
        return $this->hasMany('App\Models\Answer');
    }

    /**
     * The categories that belong to the question.
     */
    public function categories()
    {
        return $this->belongsToMany('App\Models\Category');
    }

    /**
     * Get the users for the question.
     */
    public function users()
    {
        return $this->belongsToMany('App\Models\User');
    }

    /**
     * Get the users for the question.
     */
    public function questionsUsers()
    {
        return $this->hasMany('App\Models\QuestionUser');
    }
}
