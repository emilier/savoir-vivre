<?php
namespace App\Repositories;
use App\Models\Question;
use App\Models\QuestionUser;
use App\Http\Resources\Rule as RuleResource;

class RuleRepository
{
    public function getRules()
    {
        // Get rules
        $rules = Question::with('categories');

        if (!empty(auth('api')->user())) {
            $rules = $rules->with(['questionsUsers' => function($query) {
                $query->where('user_id', auth('api')->user()->id);
            }]);
        }

        $rules = $rules->latest()
            ->paginate(10);


        // Return collection of rules as a resource
        return RuleResource::collection($rules);
    }

    public function getFavourites()
    {
        // Get favourites
        $favourites = Question::whereHas('questionsUsers', function($query) {
            $query->where('user_id', auth('api')->user()->id);
            $query->whereFavourite(true);
        })
        ->with('categories')
        ->with('questionsUsers')
        ->latest()
        ->paginate(10);

        // Return collection of rules as a resource
        return RuleResource::collection($favourites);
    }

    /**
     * Search.
     *
     * @param \Illuminate\Http\Request $request
     */
    public function search($request)
    {
//        $ads = Ad::query();
//        if($request->region != 0) {
//            $ads = Ad::whereHas('region', function ($query) use ($request) {
//                $query->where('regions.id', $request->region);
//            })->when($request->departement != 0, function ($query) use ($request) {
//                return $query->where('departement', $request->departement);
//            })->when($request->commune != 0, function ($query) use ($request) {
//                return $query->where('commune', $request->commune);
//            });
//        }
//        if($request->category != 0) {
//            $ads->whereHas('category', function ($query) use ($request) {
//                $query->where('categories.id', $request->category);
//            });
//        }
//        return $ads->with('category', 'photos')
//            ->whereActive(true)
//            ->latest()
//            ->paginate(3);
    }

    public function getSingleRule($id)
    {
        $rule = Question::with('categories')
            ->where('id', $id);

        if (!empty(auth('api')->user())) {
            $rule = $rule->with(['questionsUsers' => function($query) {
                $query->where('user_id', auth('api')->user()->id);
            }]);
        }

        $rule = $rule->first();

        return new RuleResource($rule);
    }

    private function getQuestionUser($id) {
        $questionUser = QuestionUser::where('question_id', $id)
            ->where('user_id', auth('api')->user()->id)
            ->first();

        if (empty($questionUser)) {
            Question::find($id)->users()->attach(auth('api')->user()->id);
            $questionUser = QuestionUser::where('question_id', $id)
                ->where('user_id', auth('api')->user()->id)
                ->first();
        }

        return $questionUser;
    }

    public function viewed($id)
    {
        $questionUser = $this->getQuestionUser($id);
        $questionUser->update(['viewed' => true]);

        $numberFavourites = QuestionUser::where('question_id', $id)
            ->whereViewed(true)
            ->count();

        Question::where('id', $id)
            ->update(['views' => $numberFavourites]);

        return $this->getSingleRule($id);
    }

    public function favourited($id, $request)
    {
        $favourite = $request->favourite;
        $questionUser = $this->getQuestionUser($id);
        $questionUser->update(['favourite' => $favourite]);

        $numberFavourites = QuestionUser::where('question_id', $id)
            ->where('favourite', true)
            ->count();

        Question::where('id', $id)
            ->update(['favourites' => $numberFavourites]);


        return $this->getSingleRule($id);
    }

    public function rated($id, $request)
    {
        $rate = $request->rate;
        $questionUser = $this->getQuestionUser($id);
        $questionUser->update(['rating' => $rate]);

        $average = QuestionUser::where('question_id', $id)
            ->avg('rating');

        Question::where('id', $id)
            ->update(['rating' => $average]);

        return $this->getSingleRule($id);
    }
}