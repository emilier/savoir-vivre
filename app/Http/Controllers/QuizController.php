<?php

namespace App\Http\Controllers;

use App\Models\Level;
use App\Models\Question;
use App\Http\Resources\Rule as RuleResource;
use App\Http\Requests;
use App\Models\User;
use Illuminate\Http\Request;
use Auth;

class QuizController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $levels = Level::get();
        $userDashboard = User::where('id', auth('api')->user()->id)
            ->with('level')
            ->get();

        $result = $userDashboard->add($levels);

        return new RuleResource($result);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $questions = Question::whereQuiz(true)
            ->with(['answers' => function($query) {
                $query->inRandomOrder();
            }])
            ->with('difficulty')
            ->with('categories')
            ->inRandomOrder()
            ->take(10)
            ->get();

        return RuleResource::collection($questions);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $score = $request->score;
        User::where('id', auth('api')->user()->id)
            ->increment('score', $score);

        $user = User::where('id', auth('api')->user()->id)
            ->with('level')
            ->first();


        $userScore = $user->score;
        $userLevel = $user->level->level;
        $levelScore = $user->level->score;

        if ($userScore > $levelScore) {
            $newLevel = Level::where('level', $userLevel + 1)->first();

            User::where('id', auth('api')->user()->id)
                ->update(['level_id' => $newLevel->id]);
        }

        $levels = Level::get();
        $userDashboard = User::where('id', auth('api')->user()->id)
            ->with('level')
            ->get();

        $result = $userDashboard->add($levels);

        return new RuleResource($result);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Get rule
        $rule = QuestionUser::where('question_id', $id)->where('user_id', auth()->user()->id)->get();
        $favourite = QuestionUser::findOrFail($rule[0]->id);

        if ($favourite->delete()) {
            return new RuleResource($favourite);
        }
    }
}
