<?php

namespace App\Http\Controllers;

use App\Models\Question;
use App\Http\Requests;
use App\Models\User;
use App\Models\QuestionUser;
use Illuminate\Http\Request;
use Auth;
use App\Repositories\RuleRepository;

class RuleController extends Controller
{
    /**
     * Rule repository.
     *
     * @var App\Repositories\RuleRepository
     */
    protected $repository;


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(RuleRepository $repository)
    {
        $this->repository = $repository;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->repository->getRules();
    }


    /**
     * Search rules.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  String  $slug
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        setlocale (LC_TIME, 'fr_FR');
        $ads = $this->ruleRepository->search($request);
        return view('partials.ads', compact('ads'));
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->repository->getSingleRule($id);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function viewed(Request $request, $id)
    {
        return $this->repository->viewed($id);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function favourited(Request $request, $id)
    {
        return $this->repository->favourited($id, $request);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function rated(Request $request, $id)
    {
        return $this->repository->rated($id, $request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
