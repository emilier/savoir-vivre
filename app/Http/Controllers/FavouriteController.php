<?php

namespace App\Http\Controllers;

use App\Models\Question;
use App\Models\QuestionUser;
use App\Http\Resources\Rule as RuleResource;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Repositories\RuleRepository;

class FavouriteController extends Controller
{
    /**
     * Rule repository.
     *
     * @var App\Repositories\RuleRepository
     */
    protected $repository;


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(RuleRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->repository->getFavourites();
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
            // Get rule
            $rule = QuestionUser::where('question_id', $id)
                ->where('user_id', auth()->user()->id)
                ->whereFavourite(true)
                ->first();

            if (!empty($rule)) {
                $rule->update(['favourite' => false]);
                return "ok";
            } else {
                return "not ok";
            }
    }
}
