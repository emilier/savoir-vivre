<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Ruler;
use App\Http\Resources\Rule as RuleResource;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function login(Request $request) {
        $user = User::where('email', $request->email)->first();

        if ($user) {
            if (Hash::check($request->password, $user->password)) {
                $token = $user->createToken('Laravel Password Grant Client')->accessToken;
                $response = ['token' => $token];
                return response($response, 200);
            } else {
                $response = "Password missmatch";
                return response($response, 422);
            }
        } else {
            $response = 'User does not exist';
            return response($response, 422);
        }

//        $http = new \GuzzleHttp\Client;
//
//        try {
//            $response = $http->post(config('services.passport.login_endpoint'), [
//                'form_params' => [
//                    'grant_type' => 'password',
//                    'client_id' => config('services.passport.client_id'),
//                    'client_secret' => config('services.passport.client_secret'),
//                    'username' => $request->username,
//                    'password' => $request->password,
//
//                ]
//            ]);
//            return $response->getBody();
//        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
//            if ($e->getCode() === 400) {
//                return response()->json('Invalid request. Please enter an email or a password.', $e->getCode());
//            } else if ($e->getCode() === 401) {
//                return response()->json('Your credentials are incorrect. Please try again.', $e->getCode());
//            }
//            return response()->json('Something went wrong on the server.', $e->getCode());
//        }
    }

    public function register(Request $request, $id) {
        $validator = Validator::make($request->all(), [
            'pseudo' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
            'gender' => 'required|string|min:4|max:6',
        ]);

        if ($validator->fails()) {
            return response(['errors'=>$validator->errors()->all()], 422);
        }

        $request['password'] = Hash::make($request['password']);
        $user = User::create($request->toArray());

        $token = $user->createToken('Laravel Password Grant Client')->accessToken;
        $response = ['token' => $token];

        return response($response, 200);

//        $request->validate([
//            'pseudo' => ['required', 'string', 'max:255'],
//            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
//            'password' => ['required', 'string', 'min:6'],
//        ]);

//        return User::create([
//            'pseudo' => $request->pseudo,
//            'email' => $request->email,
//            'password' => Hash::make($request->password),
//        ]);
    }

    public function logout() {
        auth()->user()->tokens->each(function ($token, $key) {
            $token->delete();
        });

        return response()->json('Logged at successfully.', 200);
    }

    public function getRulers() {
        // Get rulers
        $rulers = Ruler::get();

        // Return collection of rules as a resource
        return RuleResource::collection($rulers);
    }
}
