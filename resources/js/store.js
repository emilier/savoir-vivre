import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        token: localStorage.getItem("access_token") || null,
        rulers: [],
        rules: [],
        rule: [],
        favourites: [],
        dashboard: [],
        quiz: [],
        profile: [],
    },
    getters: {
        loggedIn(state) {
            return state.token !== null;
        }
    },
    mutations: {
        retrieveToken(state, token) {
            state.token = token;
        },
        destroyToken(state) {
            state.token = null;
        },
        clearFavourites(state) {
            state.favourites = [];
        },
        setRulers(state, data) {
            state.rulers = data;
        },
        setRules(state, data) {
            state.rules = data;
        },
        setRule(state, data) {
            state.rule = data;
        },
        setFavourites(state, data) {
            state.favourites = data;
        },
        setDashboard(state, data) {
            state.dashboard = data;
        },
        setQuiz(state, data) {
            state.quiz = data;
        },
        setProfile(state, data) {
            state.profile = data;
        }
    },
    actions: {
        register(context, credentials) {
            return new Promise((resolve, reject) => {
                axios.post('/api/register', {
                    email: credentials.email,
                    password: credentials.password,
                    gender: credentials.gender,
                    pseudo: credentials.pseudo,
                }).then(resp => {
                    resolve(resp);
                    return new Promise((resolve, reject) => {
                        axios.post('/api/login', {
                            email: credentials.email,
                            password: credentials.password,
                        }).then(response => {
                            const token = response.data.token;
                            localStorage.setItem("access_token", token);
                            context.commit("retrieveToken", token);
                            resolve(response);
                        }).catch(error => {
                            reject(error);
                        })
                    });
                }).catch(error => {
                    reject(error);
                })
            });
        },
        retrieveToken(context, credentials) {
            return new Promise((resolve, reject) => {
                axios.post('/api/login', {
                    email: credentials.email,
                    password: credentials.password,
                }).then(response => {
                    const token = response.data.token;
                    localStorage.setItem("access_token", token);
                    context.commit("retrieveToken", token);
                    resolve(response);
                }).catch(error => {
                    reject(error);
                })
            });
        },
        destroyToken(context) {
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token;
            if (context.getters.loggedIn) {
                return new Promise((resolve, reject) => {
                    axios.post('/api/logout').then(response => {
                        localStorage.removeItem("access_token");
                        context.commit("destroyToken");
                        resolve(response);
                    }).catch(error => {
                        localStorage.removeItem("access_token");
                        context.commit("destroyToken");
                        reject(error);
                    })
                });
            }
        },
        clearFavourites(context) {
            context.commit("clearFavourites");
        },
        async getRulers(context) {
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token;
            let data = (await axios.get('/api/rulers')).data;
            context.commit("setRulers", data);
        },
        async getRules(context, credentials) {
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token;

            const url = credentials.url;
            let data = (await axios.get(url)).data;
            context.commit("setRules", data);
        },
        async getRule(context, credentials) {
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token;
            const ruleID = credentials.id;
            let data = (await axios.get('/api/rule/' + ruleID)).data;
            context.commit("setRule", data);
        },
        isViewed(context, credentials) {
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token;
            const rule = credentials.rule;
            return new Promise((resolve, reject) => {
                axios.post('/api/rule/' + rule + '/viewed/').then(response => {
                    context.commit("setRule", response.data);
                    resolve(response);
                }).catch(error => {
                    reject(error);
                })
            });
        },
        isFavourited(context, credentials) {
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token;
            const rule = credentials.rule;
            return new Promise((resolve, reject) => {
                axios.post('/api/rule/' + rule + '/favourited', {
                    favourite: credentials.favourite,
                }).then(response => {
                    context.commit("setRule", response.data);
                    resolve(response);
                }).catch(error => {
                    reject(error);
                })
            });
        },
        isRated(context, credentials) {
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token;
            const rule = credentials.rule;
            return new Promise((resolve, reject) => {
                axios.post('/api/rule/' + rule + '/rated', {
                    rate: credentials.rate,
                }).then(response => {
                    context.commit("setRule", response.data);
                    resolve(response);
                }).catch(error => {
                    reject(error);
                })
            });
        },
        async getFavourites(context, credentials) {
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token;

            const url = credentials.url;
            let data = (await axios.get(url)).data;
            context.commit("setFavourites", data);
        },
        deleteFavourite(context, credentials) {
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token;
            const rule = credentials.rule;
            return new Promise((resolve, reject) => {
                axios.post('/api/rule/' + rule + '/favourited', {
                    favourite: credentials.favourite,
                }).then(response => {
                    context.commit("setRule", response.data);
                    resolve(response);
                }).catch(error => {
                    reject(error);
                })
            });
        },
        async getDashboard(context) {
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token;
            let data = (await axios.get('/api/dashboard')).data;
            context.commit("setDashboard", data);
        },
        async getQuiz(context) {
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token;
            let data = (await axios.get('/api/quiz')).data;
            context.commit("setQuiz", data);
        },
        saveScore(context, credentials) {
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token;
            return new Promise((resolve, reject) => {
                axios.post('/api/quiz', {
                    score: credentials.score
                }).then(resp => {
                    resolve(resp);
                }).catch(error => {
                    reject(error);
                })
            });
        },
        async getProfile(context) {
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token;

            let data = (await axios.get('/api/profile')).data;
            context.commit("setProfile", data);
        },
    }
})