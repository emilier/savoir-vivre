import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter);
/*
    Import des composants
 */
import Auth from './components/Auth.vue';
import Home from './components/Home.vue';
import RuleSingle from './components/RuleSingle.vue';
import Favourite from './components/Favourite.vue';
import Dashboard from './components/Dashboard.vue';
import Quiz from './components/Quiz.vue';
import Profile from './components/Profile.vue';


// 2. Définition des routes
// Chaque route doit être mappée à un composant
const routes = [
    {
        path: '/auth',
        name: 'auth',
        component: Auth,
        meta: {
            title: 'Authentification Savoir-vivre - Application des bonnes manières à la française',
            requiresVisitor: true,
        }
    },
    {
        path: '/rules',
        name: 'home',
        component: Home,
        meta: {
            title: 'Accueil Savoir-vivre - Application des bonnes manières à la française',
            requiresAuth: false,
        }
    },
    {
        path: '/',
        redirect: { name: 'home' }
    },
    {
        path: '/rules/:id',
        name: 'rule',
        component: RuleSingle,
        meta: {
            title: 'Règle de savoir-vivre - Application des bonnes manières à la française',
            requiresAuth: false,
            transitionName: 'slide',
        }
    },
    {
        path: '/favourites',
        name: 'favourites',
        component: Favourite,
        meta: {
            title: 'Favoris Savoir-vivre - Application des bonnes manières à la française',
            requiresAuth: true,
        }
    },
    {
        path: '/dashboard',
        name: 'dashboard',
        component: Dashboard,
        meta: {
            title: 'Tableau de board Savoir-vivre - Application des bonnes manières à la française',
            requiresAuth: true,
        }
    },
    {
        path: '/dashboard/quiz',
        name: 'quiz',
        component: Quiz,
        meta: {
            title: 'Quiz Savoir-vivre - Application des bonnes manières à la française',
            requiresAuth: true,
            transitionName: 'slide',
        }
    },
    {
        path: '/profile',
        name: 'profile',
        component: Profile,
        meta: {
            title: 'Profil Savoir-vivre - Application des bonnes manières à la française',
            requiresAuth: true,
        }
    }
];

export default new VueRouter({
    history: true,
    mode: 'history',
    routes
})