<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->group(function () {
    Route::post('/rule/{id}/viewed', 'RuleController@viewed');
    Route::post('/rule/{id}/favourited', 'RuleController@favourited');
    Route::post('/rule/{id}/rated', 'RuleController@rated');

    Route::get('favourites', 'FavouriteController@index');
    Route::delete('favourites/{id}', 'FavouriteController@destroy');

    Route::get('/dashboard', 'QuizController@index');
    Route::get('/quiz', 'QuizController@show');
    Route::post('/quiz', 'QuizController@update');

    Route::get('/profile', function (Request $request) {
        return $request->user();
    });

    Route::post('/logout', 'AuthController@logout');
});
Route::get('rules', 'RuleController@index');
Route::get('/rule/{id}', 'RuleController@show');

Route::post('/login', 'AuthController@login')->name('login');

Route::post('/register', 'AuthController@register')->name('register');
Route::get('/rulers', 'AuthController@getRulers');
